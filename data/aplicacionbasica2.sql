
create database aplicacionbasica2;
use aplicacionbasica2;

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `texto` varchar(300) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `foto` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `noticias` (`id`, `titulo`, `texto`, `fecha`, `foto`) VALUES
(1, 'titulo', 'texto de la noticia', '2018-09-14 11:04:06', '');

ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);
